// server.jS

const express      = require('express');
const MongoClient  = require('mongodb').MongoClient;
const bodyParser   = require('body-parser');
const db           = require('./config/db');
const app          = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

MongoClient.connect(db.url, { useNewUrlParser: true }, (err, client) => {
	if (err) return console.log(err);

	var database = client.db('notif');

	const dbModule = require('./middleware/db.js');

	dbModule.initDb(database);

	require('./app/routes/routes')(app);

	app.get('/', (req, res) => {
		console.log("Test request made!")
		res.json({"message": "Welcome to the notif!", "returnCode": 200});
	});

	app.listen(db.port, () => {
		console.log('We are live on ' + db.port);
	});
});
